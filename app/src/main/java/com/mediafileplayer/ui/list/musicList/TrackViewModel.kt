package com.mediafileplayer.ui.list.musicList

import android.content.ContentResolver
import android.provider.MediaStore
import androidx.lifecycle.viewModelScope
import androidx.viewpager2.widget.ViewPager2
import com.mediafileplayer.BR
import com.mediafileplayer.R
import com.mediafileplayer.bean.MediaBean
import com.mediafileplayer.bean.PlayerBean
import com.mediafileplayer.bean.SongBean
import com.mediafileplayer.bean.VideoBean
import com.mediafileplayer.constant.AppConstant
import com.mediafileplayer.constant.ClickActionEvents
import com.mediafileplayer.ui.base.FragmentBaseViewModel
import com.mediafileplayer.utils.getLongValue
import com.mediafileplayer.utils.getStringValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.OnItemBind
import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import java.util.*
import javax.inject.Inject

class TrackViewModel @Inject constructor(
    private val contentResolver: ContentResolver,
    private val mediaList: MutableList<PlayerBean>,
) : FragmentBaseViewModel() {
    private var videoJob: Job? = null
    private var songsJob: Job? = null
    val itemPagerView =
        ItemBinding.of<TabItem>(BR.data, R.layout.adapter_media_files).bindExtra(BR.viewModel, this)
    val itemView = OnItemBind<MediaBean> { itemBinding, _, item ->
        itemBinding.set(BR.data, getChildLayout(item)).bindExtra(BR.viewModel, this)
    }
    val dataList = DiffObservableList(TracksDiffUtil())
    val tabItem = mutableListOf<TabItem>()
    private var currentPosition = -1
    val callback: ViewPager2.OnPageChangeCallback

    init {
        tabItem.add(TabItem(R.drawable.ic_songs, R.string.songs))
        tabItem.add(TabItem(R.drawable.ic_video, R.string.videos))

        callback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (currentPosition != position || dataList.size == 0) {
                    dataList.update(Collections.emptyList())
                    currentPosition = position
                    when (position) {
                        AppConstant.SONGS -> loadSongsData()
                        AppConstant.VIDEOS -> loadVideos()
                    }
                    activityBaseViewModel.itemClickType = position
                }
            }
        }
    }

    private fun getChildLayout(item: MediaBean?): Int {
        return if (item is VideoBean) R.layout.adapter_video_item else R.layout.adapter_media_item
    }

    fun loadSongsData() {
        videoJob?.cancel()
        activityBaseViewModel.toolbarTitle.value = R.string.audio_tracks
        songsJob = viewModelScope.launch(Dispatchers.IO) {
            showProgress.postValue(true)
            val projection = arrayOf(
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.MIME_TYPE
            )
            val list = ArrayList<MediaBean>()
            try {
                val cursor = contentResolver.query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    null,
                    null,
                    "${MediaStore.Audio.Media.DISPLAY_NAME} ASC"
                )
                if (cursor != null && !cursor.isClosed) {
                    while (cursor.moveToNext()) {
                        val songData = SongBean(
                            cursor.getLongValue(MediaStore.Audio.Media._ID),
                            cursor.getStringValue(MediaStore.Audio.Media.TITLE),
                            cursor.getStringValue(MediaStore.Audio.Media.DISPLAY_NAME),
                            artist = cursor.getStringValue(MediaStore.Audio.Media.ARTIST),
                            id = cursor.getLongValue(MediaStore.Audio.Media.ALBUM_ID),
                            count = 0,
                            mediaPath = cursor.getStringValue(MediaStore.Audio.Media.DATA),
                            mimeType = cursor.getStringValue(MediaStore.Audio.Media.MIME_TYPE)
                        )
                        list.add(songData)
                    }
                    cursor.close()
                }
            } catch (_: SecurityException) {
            }

            withContext(Dispatchers.Main) {
                dataList.update(list)
                showProgress.value = false
            }
        }
    }

    fun loadVideos() {
        songsJob?.cancel()
        activityBaseViewModel.toolbarTitle.value = R.string.video_tracks
        videoJob = viewModelScope.launch(Dispatchers.IO) {
            showProgress.postValue(true)
            val projection = arrayOf(
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.MIME_TYPE
            )

            val cursor = contentResolver.query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                "${MediaStore.Video.Media.DISPLAY_NAME} ASC"
            )
            val list = ArrayList<MediaBean>()
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    val id = cursor.getLongValue(MediaStore.Video.Media._ID)
                    val videoBean = VideoBean(
                        id,
                        cursor.getStringValue(MediaStore.Video.Media.TITLE),
                        cursor.getStringValue(MediaStore.Video.Media.DISPLAY_NAME),
                        mediaPath = cursor.getStringValue(MediaStore.Video.Media.DATA),
                        mimeType = cursor.getStringValue(MediaStore.Video.Media.MIME_TYPE)
                    )
                    list.add(videoBean)
                }
                cursor.close()
            }
            withContext(Dispatchers.Main) {
                dataList.update(list)
                showProgress.value = false
            }
        }
    }

    fun itemClick(data: MediaBean) {
        if (data is SongBean) {
            activityBaseViewModel.playMedia = data
            activityBaseViewModel.playPosition = dataList.indexOf(data)
            mediaList.apply {
                clear()
                for (media in dataList)
                    add(media as SongBean)
            }
            activityBaseViewModel.screenNavigation.value = ClickActionEvents.MUSIC_PLAYER
        } else if (data is VideoBean) {
            activityBaseViewModel.playMedia = data
            activityBaseViewModel.playPosition = dataList.indexOf(data)
            mediaList.apply {
                clear()
                for (media in dataList)
                    add(media as VideoBean)
            }
            activityBaseViewModel.screenNavigation.value = ClickActionEvents.VIDEO_PLAYER
        }
    }

    override fun onCleared() {
        dataList.update(mutableListOf())
        super.onCleared()
    }

    fun clickAction(@ClickActionEvents value: String) {
        activityBaseViewModel.screenNavigation.value = value
    }
}