package com.mediafileplayer.ui.list.playList

import android.os.Bundle
import com.mediafileplayer.R
import com.mediafileplayer.databinding.ActivityPlayListBinding
import com.mediafileplayer.ui.base.AppBaseActivity

class PlayListActivity : AppBaseActivity<ActivityPlayListBinding, PlayListViewModel>() {

    override fun provideViewModelClass(): Class<PlayListViewModel> = PlayListViewModel::class.java

    override val layoutId = R.layout.activity_play_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(dataBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel.screenNavigation.observe(this) {
            onBackPressed()
        }
    }
}