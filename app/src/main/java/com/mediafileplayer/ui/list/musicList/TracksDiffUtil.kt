package com.mediafileplayer.ui.list.musicList

import android.text.TextUtils
import androidx.recyclerview.widget.DiffUtil
import com.mediafileplayer.bean.MediaBean

class TracksDiffUtil : DiffUtil.ItemCallback<MediaBean>() {
    override fun areItemsTheSame(oldItem: MediaBean, newItem: MediaBean): Boolean {
        return oldItem.rowId == newItem.rowId
    }

    override fun areContentsTheSame(oldItem: MediaBean, newItem: MediaBean): Boolean {
        return TextUtils.equals(oldItem.name, newItem.name)
    }
}