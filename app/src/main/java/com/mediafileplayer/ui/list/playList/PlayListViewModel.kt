package com.mediafileplayer.ui.list.playList

import androidx.databinding.ObservableLong
import androidx.media3.common.Player
import androidx.media3.common.Tracks
import androidx.media3.exoplayer.ExoPlayer
import com.mediafileplayer.BR
import com.mediafileplayer.R
import com.mediafileplayer.bean.PlayerBean
import com.mediafileplayer.ui.base.FragmentBaseViewModel
import com.mediafileplayer.utils.SingleLiveEvent
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import java.util.*
import javax.inject.Inject

/**
 * Created by akashjain on 18/03/18.
 */

class PlayListViewModel @Inject constructor(
    val player: ExoPlayer,
    private val mediaList: MutableList<PlayerBean>
) : FragmentBaseViewModel() {
    val screenNavigation = SingleLiveEvent<Void>()
    private val listener: Player.Listener
    val currentPosition = ObservableLong()
    val itemView =
        ItemBinding.of<PlayerBean>(BR.data, R.layout.adapter_play_list_item)
            .bindExtra(BR.viewModel, this)
    val dataList = DiffObservableList(PlayListDiffUtil()).apply {
        update(mediaList)
    }

    init {
        currentPosition.set(mediaList[player.currentMediaItemIndex].rowId)
        listener = object : Player.Listener {
            override fun onTracksChanged(tracks: Tracks) {
                super.onTracksChanged(tracks)
                currentPosition.set(mediaList[player.currentMediaItemIndex].rowId)
                dataList.update(Collections.emptyList())
                dataList.update(mediaList)
            }
        }
        player.addListener(listener)
    }

    fun itemClick(data: PlayerBean) {
        player.apply {
            var indexOf = mediaList.indexOf(data)
            if (indexOf == -1)
                indexOf = 0
            seekTo(indexOf, 0)
            playWhenReady = true
        }
        screenNavigation.call()
    }

    fun isPlaying(data: PlayerBean): Boolean {
        return data.rowId == currentPosition.get()
    }
}