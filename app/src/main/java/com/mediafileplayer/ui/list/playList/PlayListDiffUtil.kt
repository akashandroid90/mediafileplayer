package com.mediafileplayer.ui.list.playList

import android.text.TextUtils
import androidx.recyclerview.widget.DiffUtil
import com.mediafileplayer.bean.PlayerBean

class PlayListDiffUtil : DiffUtil.ItemCallback<PlayerBean>() {
    override fun areItemsTheSame(oldItem: PlayerBean, newItem: PlayerBean): Boolean {
        return oldItem.rowId == newItem.rowId
    }

    override fun areContentsTheSame(oldItem: PlayerBean, newItem: PlayerBean): Boolean {
        return TextUtils.equals(oldItem.name, newItem.name)
    }
}