package com.mediafileplayer.ui.list.musicList

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class TabItem(
    @DrawableRes val image: Int,
    @StringRes val title: Int,
    val showAdd: Boolean = false
)