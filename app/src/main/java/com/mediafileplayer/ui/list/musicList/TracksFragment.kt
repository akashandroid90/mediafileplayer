package com.mediafileplayer.ui.list.musicList

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import com.mediafileplayer.R
import com.mediafileplayer.databinding.FragmentTracksBinding
import com.mediafileplayer.ui.base.AppBaseFragment
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class TracksFragment : AppBaseFragment<FragmentTracksBinding, TrackViewModel>() {
    override fun provideViewModelClass(): Class<TrackViewModel> = TrackViewModel::class.java

    override fun layoutId(): Int = R.layout.fragment_tracks

    private var tabLayoutMediator: TabLayoutMediator? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding?.vpFiles?.let { pager ->
            viewBinding?.tabLayout?.let {
                tabLayoutMediator = TabLayoutMediator(
                    it, pager
                ) { tab, position ->
                    tab.setText(viewModel.tabItem[position].title)
                    tab.setIcon(viewModel.tabItem[position].image)
                }
                tabLayoutMediator?.attach()
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            fetchDataForTIRAMISUWithPermissionCheck()
        } else {
            fetchDataWithPermissionCheck()
        }
    }

    override fun onDestroyView() {
        tabLayoutMediator?.detach()
        context?.let { Glide.get(it).clearMemory() }
        super.onDestroyView()
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    @NeedsPermission(
        Manifest.permission.POST_NOTIFICATIONS,
        Manifest.permission.READ_MEDIA_IMAGES,
        Manifest.permission.READ_MEDIA_AUDIO,
        Manifest.permission.READ_MEDIA_VIDEO
    )
    fun fetchDataForTIRAMISU() {
        viewBinding?.vpFiles?.let {
            viewModel.callback.onPageSelected(it.currentItem)
        }
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun fetchData() {
        viewBinding?.vpFiles?.let {
            viewModel.callback.onPageSelected(it.currentItem)
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    @OnPermissionDenied(
        Manifest.permission.POST_NOTIFICATIONS,
        Manifest.permission.READ_MEDIA_IMAGES,
        Manifest.permission.READ_MEDIA_AUDIO,
        Manifest.permission.READ_MEDIA_VIDEO,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    fun onPermissionDenied() {
        Toast.makeText(
            context,
            "We need all permissions. Please give all permissions from application settings",
            Toast.LENGTH_SHORT
        ).show()
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}