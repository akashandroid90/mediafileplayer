package com.mediafileplayer.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.mediafileplayer.R
import com.mediafileplayer.bean.SongBean
import com.mediafileplayer.bean.VideoBean
import com.mediafileplayer.constant.AppConstant
import com.mediafileplayer.constant.ClickActionEvents
import com.mediafileplayer.databinding.ActivityHomeBinding
import com.mediafileplayer.ui.base.AppBaseActivity
import com.mediafileplayer.ui.list.musicList.TracksFragment
import com.mediafileplayer.ui.player.musicPlayer.MusicPlayerActivity
import com.mediafileplayer.ui.player.videoPlayer.VideoPlayerActivity
import com.mediafileplayer.utils.TransitionManager
import com.mediafileplayer.utils.appOnPlayStore
import com.mediafileplayer.utils.appShare

class HomeActivity : AppBaseActivity<ActivityHomeBinding, HomeViewModel>() {
    override val layoutId: Int = R.layout.activity_home
    override fun provideViewModelClass() = HomeViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.showToolBar.observe(this) {
            if (it)
                supportActionBar?.show()
            else supportActionBar?.hide()
        }

        viewModel.toolbarTitle.observe(this) {
            dataBinding.toolbar.setTitle(it)
        }

        viewModel.screenNavigation.observe(this) {
            when (it) {
                ClickActionEvents.HOME -> {
                    TransitionManager.replaceFragment(
                        this,
                        TracksFragment(),
                        R.id.fl_container,
                        false
                    )
                    viewModel.showToolBar.value = true
                }

                ClickActionEvents.MUSIC_PLAYER -> {
                    val intent = Intent(this, MusicPlayerActivity::class.java)
                    intent.putExtra(AppConstant.FILE_DATA, viewModel.playMedia as SongBean)
                    intent.putExtra(AppConstant.PLAYLIST_POSITION, viewModel.playPosition)
                    startActivity(intent)
                }

                ClickActionEvents.VIDEO_PLAYER -> {
                    val intent = Intent(this, VideoPlayerActivity::class.java)
                    intent.putExtra(AppConstant.FILE_DATA, viewModel.playMedia as VideoBean)
                    intent.putExtra(AppConstant.PLAYLIST_POSITION, viewModel.playPosition)
                    startActivity(intent)
                }
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        setSupportActionBar(dataBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val stringExtra = intent.getStringExtra(AppConstant.NOTIFICATION_DATA)
        if (stringExtra != null) {
            viewModel.screenNavigation.value = stringExtra
        } else viewModel.screenNavigation.value = ClickActionEvents.HOME
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.rate_us -> {
                appOnPlayStore()
            }

            R.id.share -> {
                appShare()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}