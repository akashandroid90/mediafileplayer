package com.mediafileplayer.ui.home

import com.mediafileplayer.bean.MediaBean
import com.mediafileplayer.constant.ClickActionEvents
import com.mediafileplayer.ui.base.BaseViewModel
import com.mediafileplayer.utils.SingleLiveEvent
import javax.inject.Inject

class HomeViewModel @Inject constructor() :
    BaseViewModel() {
    val showToolBar = SingleLiveEvent<Boolean>()
    val toolbarTitle = SingleLiveEvent<Int>()

    @ClickActionEvents
    val screenNavigation = SingleLiveEvent<String>()

    var itemClickType: Int = -1
    var playMedia: MediaBean? = null
    var playPosition: Int? = null
}