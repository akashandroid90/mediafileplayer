package com.mediafileplayer.ui.player.videoPlayer

import com.mediafileplayer.R
import com.mediafileplayer.databinding.ActivityVideoPlayerBinding
import com.mediafileplayer.ui.player.AppMediaPlayer
import com.mediafileplayer.ui.player.musicPlayer.MediaPlayerViewModel

class VideoPlayerActivity : AppMediaPlayer<ActivityVideoPlayerBinding>() {
    override val layoutId: Int
        get() = R.layout.activity_video_player

    override fun provideViewModelClass() = MediaPlayerViewModel::class.java

    override fun onPause() {
        dataBinding.playerView.onPause()
        super.onPause()
    }

    override fun onResume() {
        dataBinding.playerView.onResume()
        super.onResume()
    }
}