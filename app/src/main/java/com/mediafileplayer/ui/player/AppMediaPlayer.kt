package com.mediafileplayer.ui.player

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.mediafileplayer.bean.VideoBean
import com.mediafileplayer.constant.AppConstant
import com.mediafileplayer.notification.PlayerService
import com.mediafileplayer.ui.base.AppBaseActivity
import com.mediafileplayer.ui.player.musicPlayer.MediaPlayerViewModel

abstract class AppMediaPlayer<B : ViewDataBinding> :
    AppBaseActivity<B, MediaPlayerViewModel>() {
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        if (viewModel.mediaList.isEmpty()) {
            val data: VideoBean? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                intent.getParcelableExtra(AppConstant.FILE_DATA, VideoBean::class.java)
            } else {
                intent.getParcelableExtra(AppConstant.FILE_DATA)
            }
            data?.let {
                viewModel.mediaList.apply {
                    clear()
                    add(it)
                }
            }
        }
        if (!intent.getBooleanExtra(AppConstant.FROM_NOTIFICATION, false))
            viewModel.createPlayList(intent.getIntExtra(AppConstant.PLAYLIST_POSITION, 0))
        intent.removeExtra(AppConstant.FILE_DATA)
        intent.removeExtra(AppConstant.PLAYLIST_POSITION)
        intent.removeExtra(AppConstant.FROM_NOTIFICATION)
        startService(Intent(this, PlayerService::class.java))
    }
}