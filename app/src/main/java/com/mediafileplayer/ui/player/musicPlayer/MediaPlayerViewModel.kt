package com.mediafileplayer.ui.player.musicPlayer

import android.net.Uri
import androidx.annotation.OptIn
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import androidx.media3.common.Tracks
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.MediaSource
import com.mediafileplayer.bean.PlayerBean
import com.mediafileplayer.ui.base.BaseViewModel
import com.mediafileplayer.utils.Logger
import com.mediafileplayer.utils.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MediaPlayerViewModel @Inject constructor(
    val player: ExoPlayer,
    val mediaList: MutableList<PlayerBean>,
    private val mediaSourceFactory: MediaSource.Factory
) : BaseViewModel() {
    private val listeners = mutableListOf<Any>()
    val currentTitle = ObservableField<String>()
    val audioSessionId = SingleLiveEvent<Int>()

    init {
        addListener(object : Player.Listener {
            override fun onPlayerError(error: PlaybackException) {
                super.onPlayerError(error)
                Logger.error(error.message, error)
            }

            @OptIn(UnstableApi::class)
            override fun onTracksChanged(tracks: Tracks) {
                super.onTracksChanged(tracks)

                if (tracks.groups.isNotEmpty()) {
                    val playerBean = mediaList[player.currentMediaItemIndex]
                    currentTitle.set(playerBean.name)
                }
                audioSessionId.value = player.audioSessionId
            }
        })
        currentTitle.set(mediaList[player.currentMediaItemIndex].name)
    }

    private fun addListener(callback: Any) {
        when (callback) {
            is Player.Listener -> {
                player.addListener(callback)
            }
        }
        listeners.add(callback)
    }

    @OptIn(UnstableApi::class)
    fun createPlayList(position: Int?) {
        viewModelScope.launch(Dispatchers.IO) {
            val playList: MutableList<MediaSource> = mutableListOf()
            for (data in mediaList) {
//     Create a data source factory.
//     Create a progressive media source pointing to a stream uri.
//        val mediaBean = mViewModel.data!!.value!![mViewModel.position]
                playList.add(
                    mediaSourceFactory
                        .createMediaSource(MediaItem.fromUri(Uri.parse(data.mediaPath)))
                )
            }
            withContext(Dispatchers.Main) {
                player.apply {
                    setMediaSources(playList)
                    prepare()
                    seekTo(position ?: 0, 0)
                    playWhenReady = true
                }
            }
        }
    }


    override fun onCleared() {
        for (callback in listeners) {
            when (callback) {
                is Player.Listener -> {
                    player.removeListener(callback)
                }
            }
        }
        listeners.clear()
        super.onCleared()
    }
}