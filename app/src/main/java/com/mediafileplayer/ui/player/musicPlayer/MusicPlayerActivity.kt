package com.mediafileplayer.ui.player.musicPlayer

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.OptIn
import androidx.media3.common.util.UnstableApi
import com.cleveroad.audiovisualization.DbmHandler
import com.mediafileplayer.R
import com.mediafileplayer.databinding.ActivityMusicPlayerBinding
import com.mediafileplayer.ui.list.playList.PlayListActivity
import com.mediafileplayer.ui.player.AppMediaPlayer
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class MusicPlayerActivity : AppMediaPlayer<ActivityMusicPlayerBinding>() {
    override val layoutId: Int
        get() = R.layout.activity_music_player

    override fun provideViewModelClass() = MediaPlayerViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.audioSessionId.observe(this) {
            initializeVisualizerWithPermissionCheck()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        setSupportActionBar(dataBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (viewModel.player.isPlaying)
            initializeVisualizerWithPermissionCheck()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuItem = menu.add(Menu.NONE, R.id.id_music_playlist, Menu.NONE, R.string.playlist)
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
        menuItem.setIcon(R.drawable.ic_queue_music)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.id_music_playlist) {
            startActivity(Intent(this, PlayListActivity::class.java))
            true
        } else
            super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        dataBinding.visualizerView.onResume()
    }

    override fun onPause() {
        super.onPause()
        dataBinding.visualizerView.onPause()
    }

    @OptIn(UnstableApi::class)
    @NeedsPermission(Manifest.permission.RECORD_AUDIO, Manifest.permission.MODIFY_AUDIO_SETTINGS)
    fun initializeVisualizer() {
        dataBinding.visualizerView.linkTo(
            DbmHandler.Factory.newVisualizerHandler(
                this,
                viewModel.player.audioSessionId
            )
        )
        dataBinding.visualizerView.onResume()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}