package com.mediafileplayer.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.mediafileplayer.BR
import com.mediafileplayer.ui.home.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class AppBaseFragment<VB : ViewDataBinding, VM : FragmentBaseViewModel> :
    DaggerFragment() {

    abstract fun provideViewModelClass(): Class<VM>
    abstract fun layoutId(): Int

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected var viewBinding: VB? = null
    protected lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[provideViewModelClass()]
        activity?.let {
            viewModel.activityBaseViewModel =
                ViewModelProvider(it, viewModelFactory)[HomeViewModel::class.java]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DataBindingUtil.inflate<VB>(inflater, layoutId(), container, false).apply {
        lifecycleOwner = this@AppBaseFragment
        viewBinding = this
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding?.setVariable(BR.viewModel, viewModel)
        viewBinding?.executePendingBindings()
    }

    override fun onDestroyView() {
        viewBinding?.unbind()
        viewBinding = null
        super.onDestroyView()
    }
}