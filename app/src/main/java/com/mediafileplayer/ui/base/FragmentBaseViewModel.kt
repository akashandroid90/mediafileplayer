package com.mediafileplayer.ui.base

import androidx.databinding.ObservableBoolean
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.mediafileplayer.ui.home.HomeViewModel

abstract class FragmentBaseViewModel :
    BaseViewModel() {
    lateinit var activityBaseViewModel: HomeViewModel
    val showAdView = ObservableBoolean(true)

    fun getAdRequest(): AdRequest = AdRequest.Builder().build()

    fun getAdListener(): AdListener {
        return object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                showAdView.set(true)
            }
        }
    }
}