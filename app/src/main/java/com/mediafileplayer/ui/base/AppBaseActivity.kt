package com.mediafileplayer.ui.base

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.mediafileplayer.BR
import com.mediafileplayer.R
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class AppBaseActivity<B : ViewDataBinding, V : BaseViewModel> : DaggerAppCompatActivity() {
    protected lateinit var viewModel: V
    protected lateinit var dataBinding: B
    abstract fun provideViewModelClass(): Class<V>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    protected abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[provideViewModelClass()]
        dataBinding = DataBindingUtil.setContentView(this, layoutId)
        dataBinding.setVariable(BR.viewModel, viewModel)
        dataBinding.executePendingBindings()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_player, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        dataBinding.unbind()
        super.onDestroy()
    }
}