package com.mediafileplayer.databinding

import android.graphics.drawable.Drawable
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mediafileplayer.R
import com.mediafileplayer.bean.MediaBean
import com.mediafileplayer.utils.getBitmap
import com.mediafileplayer.utils.milliSecondsToTimer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.EmptyCoroutineContext


/**
 * BindingAdapters.kt contains DataBinding conversions and adapters
 */

@BindingAdapter(value = ["imageUrl", "placeHolder", "error", "circle"], requireAll = false)
fun loadImage(
    imageView: AppCompatImageView,
    data: MediaBean?,
    placeholder: Drawable?,
    error: Drawable?,
    circle: Boolean = false
) {
    Glide.with(imageView).apply {
        clear(imageView)
        if (circle) {
            load(placeholder).apply(RequestOptions.circleCropTransform()).into(imageView)
        } else {
            load(placeholder).into(imageView)
        }
    }
    data?.mediaPath?.let {
        CoroutineScope(EmptyCoroutineContext).launch(Dispatchers.IO) {
            val image = data.getBitmap(imageView.context)
            withContext(Dispatchers.Main) {
                val viewTarget = Glide.with(imageView).load(image)
                    .thumbnail(Glide.with(imageView).asDrawable().sizeMultiplier(0.1f))
                    .placeholder(placeholder).error(error)
                if (circle) {
                    viewTarget.apply(RequestOptions.circleCropTransform()).into(imageView)
                } else {
                    viewTarget.into(imageView)
                }
            }
        }
    }
}

@BindingAdapter("addItemDecoration")
fun addItemDecoration(view: RecyclerView, vertical: Boolean) {
    if (vertical)
        view.addItemDecoration(DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL))
    else view.addItemDecoration(
        DividerItemDecoration(
            view.context,
            DividerItemDecoration.HORIZONTAL
        )
    )
}

@BindingAdapter("trackCount")
fun trackCountWithPlural(view: AppCompatTextView, count: Int) {
    view.text = view.resources.getQuantityString(R.plurals.tracks, count, count)
}

@BindingAdapter("mediaDurationFromPath")
fun mediaDurationFromPath(view: AppCompatTextView, filePath: String) {
    val mediaMetadataRetriever = MediaMetadataRetriever()
    try {
        mediaMetadataRetriever.setDataSource(view.context, Uri.parse(filePath))
        val time =
            mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                ?.toLong() ?: 0
        view.visibility = View.VISIBLE
        view.text = time.milliSecondsToTimer()
    } catch (ex: IllegalArgumentException) {
        view.visibility = View.GONE
    } catch (ex: SecurityException) {
        view.visibility = View.GONE
    }
}