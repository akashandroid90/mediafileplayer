package com.mediafileplayer.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.MediaStore.Video
import android.util.Size
import androidx.core.app.ShareCompat
import com.mediafileplayer.R
import com.mediafileplayer.bean.AlbumBean
import com.mediafileplayer.bean.MediaBean
import com.mediafileplayer.bean.SongBean
import com.mediafileplayer.bean.VideoBean
import java.io.File

fun Context.appOnPlayStore() {
    val intent = Intent(Intent.ACTION_VIEW)
    if (Build.MANUFACTURER.equals("amazon", ignoreCase = true)) {
        intent.data = Uri.parse("amzn://apps/android?p=$packageName")
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            intent.data =
                Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=$packageName")
            startActivity(intent)
        }

    } else {
        intent.data = Uri.parse("market://details?id=$packageName")
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            intent.data =
                Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
            startActivity(intent)
        }
    }
}

fun Context.appShare() {
    val intentBuilder = ShareCompat.IntentBuilder(this)
        .setSubject(getString(R.string.app_name))
        .setType("text/plain") // most general text sharing MIME type
        .setChooserTitle("Share Using..")
    if (Build.MANUFACTURER.equals("amazon", ignoreCase = true))
        intentBuilder.setText("http://www.amazon.com/gp/mas/dl/android?p=$packageName")
    else
        intentBuilder.setText("http://play.google.com/store/apps/details?id=$packageName")
    intentBuilder.startChooser()
}

fun MediaBean.getBitmap(context: Context): Bitmap? {
    try {
        return when (this) {
            is AlbumBean, is SongBean -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val dimen = context.resources.getDimensionPixelSize(R.dimen.audio_image_size)
                    this.mediaPath?.let {
                        ThumbnailUtils.createAudioThumbnail(
                            File(it),
                            Size(dimen, dimen),
                            null
                        )
                    }
                } else {
                    val query = context.contentResolver.query(
                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                        arrayOf(MediaStore.Audio.Albums.ALBUM_ART),
                        MediaStore.Audio.Albums._ID + "=" + id,
                        null,
                        null
                    )
                    query?.apply { moveToNext() }
                        ?.let { BitmapFactory.decodeFile(query.getString(0)) }
                        .apply { query?.close() }
                }
            }

            is VideoBean -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val width =
                        context.resources.getDimensionPixelSize(R.dimen.video_image_width_size)
                    val height =
                        context.resources.getDimensionPixelSize(R.dimen.video_image_height_size)
                    this.mediaPath?.let {
                        ThumbnailUtils.createVideoThumbnail(
                            File(it),
                            Size(width, height),
                            null
                        )
                    }
                } else {
                    Video.Thumbnails.getThumbnail(
                        context.contentResolver,
                        id,
                        Video.Thumbnails.MICRO_KIND,
                        null
                    )
                }
            }

            else -> null
        }
    } catch (e: Exception) {
        return null
    }
}

fun Long.milliSecondsToTimer(): StringBuilder {
    val time = StringBuilder()
    // Convert total duration into time
    val hours = (this / (1000 * 60 * 60)).toInt()
    val minutes = (this % (1000 * 60 * 60)).toInt() / (1000 * 60)
    val seconds = (this % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
    // Add hours if there
    if (hours > 0) {
        time.append(hours).append(":")
    }
    time.append(minutes).append(":")
    // Prepending 0 to seconds if it is one digit
    if (seconds < 10) {
        time.append("0$seconds")
    } else {
        time.append(seconds)
    }
    return time
}

fun Cursor.getStringValue(key: String): String = getString(getColumnIndexOrThrow(key))

fun Cursor.getLongValue(key: String) = getLong(getColumnIndexOrThrow(key))
