package com.mediafileplayer.utils

import androidx.annotation.IntDef
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.mediafileplayer.R
import com.mediafileplayer.utils.TransitionManager.TransitionAnimation.Companion.TRANSITION_FADE_IN_OUT
import com.mediafileplayer.utils.TransitionManager.TransitionAnimation.Companion.TRANSITION_NONE
import com.mediafileplayer.utils.TransitionManager.TransitionAnimation.Companion.TRANSITION_POP
import com.mediafileplayer.utils.TransitionManager.TransitionAnimation.Companion.TRANSITION_PUSH_TO_STACK
import com.mediafileplayer.utils.TransitionManager.TransitionAnimation.Companion.TRANSITION_SLIDE_LEFT_RIGHT
import com.mediafileplayer.utils.TransitionManager.TransitionAnimation.Companion.TRANSITION_SLIDE_LEFT_RIGHT_WITHOUT_EXIT

/****
 * This class takes care of all fragment transitions and backstack management
 * Author: Lajesh Dineshkumar
 * Company: Farabi Technologies
 * Created on: 2019-09-16
 * Modified on: 2019-09-16
 *****/
object TransitionManager {

    @IntDef(
        TRANSITION_POP, TRANSITION_SLIDE_LEFT_RIGHT,
        TRANSITION_FADE_IN_OUT, TRANSITION_SLIDE_LEFT_RIGHT_WITHOUT_EXIT,
        TRANSITION_NONE, TRANSITION_PUSH_TO_STACK
    )
    annotation class TransitionAnimation {
        companion object {
            const val TRANSITION_POP = 0
            const val TRANSITION_SLIDE_LEFT_RIGHT = 1
            const val TRANSITION_FADE_IN_OUT = 2
            const val TRANSITION_SLIDE_LEFT_RIGHT_WITHOUT_EXIT = 3
            const val TRANSITION_NONE = 4
            const val TRANSITION_PUSH_TO_STACK = 5
            const val TRANSITION_FADE_IN_POP_OUT = 6

        }
    }

    /**
     * The method for replacing a fragment
     * @param activity : Parent Activity
     * @param fragment: Fragment to be added
     * @param id: Fragment container ID
     * @param addToBackStack: Flag indicating whether to add to backstack or not
     * @param animationType: Fragment transition animation type
     */
    fun replaceFragment(
        activity: AppCompatActivity,
        fragment: Fragment,
        id: Int,
        addToBackStack: Boolean, @TransitionAnimation animationType: Int = TRANSITION_NONE
    ) {
        activity.supportFragmentManager.beginTransaction().apply {
            when (animationType) {
                TRANSITION_POP -> setCustomAnimations(
                    R.anim.anim_enter,
                    R.anim.anim_exit,
                    R.anim.anim_pop_enter,
                    R.anim.anim_pop_exit
                )

                TRANSITION_FADE_IN_OUT -> setCustomAnimations(
                    R.anim.anim_frag_fade_in,
                    R.anim.anim_frag_fade_out
                )

                TRANSITION_SLIDE_LEFT_RIGHT -> setCustomAnimations(
                    R.anim.slide_in_from_rigth, R.anim.slide_out_to_left,
                    R.anim.slide_in_from_left, R.anim.slide_out_to_right
                )

                TRANSITION_SLIDE_LEFT_RIGHT_WITHOUT_EXIT -> setCustomAnimations(
                    R.anim.slide_in_from_rigth,
                    0
                )

                TRANSITION_NONE -> setCustomAnimations(0, 0)

                TRANSITION_PUSH_TO_STACK -> {
                    // do nothing
                }
            }
            if (addToBackStack)
                addToBackStack(fragment.javaClass.canonicalName)

            replace(id, fragment, fragment.javaClass.canonicalName)
            commit()
        }
    }
}