package com.mediafileplayer.constant

object AppConstant {
    const val FROM_NOTIFICATION = "from_notification"
    const val PLAYLIST_POSITION = "playlist_position"
    const val FILE_DATA = "file_data"
    const val NOTIFICATION_DATA = "notification_data"
    const val SONGS = 0
    const val VIDEOS = 1
}