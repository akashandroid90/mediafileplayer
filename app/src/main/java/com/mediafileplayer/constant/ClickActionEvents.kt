package com.mediafileplayer.constant

import androidx.annotation.StringDef

@Retention(AnnotationRetention.SOURCE)
@StringDef(
    ClickActionEvents.HOME,
    ClickActionEvents.MUSIC_PLAYER,
    ClickActionEvents.VIDEO_PLAYER
)
annotation class ClickActionEvents {
    companion object {
        const val HOME = "home"
        const val MUSIC_PLAYER = "music_player"
        const val VIDEO_PLAYER = "video_player"
        const val ADD_PLAYLIST = "add_play_list"
    }
}