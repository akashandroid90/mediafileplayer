package com.mediafileplayer.notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import androidx.core.app.TaskStackBuilder
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.ui.PlayerNotificationManager
import com.mediafileplayer.bean.PlayerBean
import com.mediafileplayer.constant.AppConstant
import com.mediafileplayer.constant.ClickActionEvents
import com.mediafileplayer.ui.player.musicPlayer.MusicPlayerActivity
import com.mediafileplayer.ui.player.videoPlayer.VideoPlayerActivity
import com.mediafileplayer.utils.getBitmap

@UnstableApi
class DescriptionAdapter(val context: Context, private val mediaList: MutableList<PlayerBean>) :
    PlayerNotificationManager.MediaDescriptionAdapter {
    override fun createCurrentContentIntent(player: Player): PendingIntent? {
        var intent: Intent? = null
        mediaList[player.currentMediaItemIndex].mimeType?.let {
            intent = if (it.contains("audio"))
                Intent(context, MusicPlayerActivity::class.java)
            else Intent(context, VideoPlayerActivity::class.java)
        }
        return intent?.let {
            it.putExtra(AppConstant.FROM_NOTIFICATION, true)
            TaskStackBuilder.create(context).run {
                // Add the intent, which inflates the back stack
                addNextIntentWithParentStack(it)
                val editIntentAt = editIntentAt(0)
                mediaList[player.currentMediaItemIndex].mimeType?.let { mimeType ->
                    editIntentAt?.putExtra(
                        AppConstant.NOTIFICATION_DATA, if (mimeType.contains("audio"))
                            ClickActionEvents.HOME
                        else ClickActionEvents.HOME
                    )
                }
                // Get the PendingIntent containing the entire back stack
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    getPendingIntent(0, PendingIntent.FLAG_MUTABLE)
                } else {
                    getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                }
            }
        }
    }

    override fun getCurrentContentText(player: Player): String? =
        mediaList[player.currentMediaItemIndex].displayName

    override fun getCurrentContentTitle(player: Player): String =
        mediaList[player.currentMediaItemIndex].name ?: ""

    override fun getCurrentLargeIcon(
        player: Player,
        callback: PlayerNotificationManager.BitmapCallback
    ): Bitmap? = mediaList[player.currentMediaItemIndex].getBitmap(context)
}