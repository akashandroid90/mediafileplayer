package com.mediafileplayer.notification

import android.app.Notification
import android.os.Build
import androidx.annotation.OptIn
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.media3.common.Player
import androidx.media3.common.util.NotificationUtil
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.PlayerNotificationManager
import com.mediafileplayer.BuildConfig
import com.mediafileplayer.R
import dagger.android.AndroidInjection
import javax.inject.Inject

class PlayerService : LifecycleService() {
    @Inject
    @UnstableApi
    lateinit var adapter: DescriptionAdapter

    @Inject
    lateinit var player: ExoPlayer

    @OptIn(UnstableApi::class)
    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
        val listener = object : PlayerNotificationManager.NotificationListener {
            override fun onNotificationCancelled(
                notificationId: Int,
                dismissedByUser: Boolean
            ) {
                super.onNotificationCancelled(notificationId, dismissedByUser)
                stopSelf()
            }

            override fun onNotificationPosted(
                notificationId: Int,
                notification: Notification,
                ongoing: Boolean
            ) {
                super.onNotificationPosted(notificationId, notification, ongoing)
                if (ongoing) {
                    startForeground(notificationId, notification)
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    stopForeground(STOP_FOREGROUND_DETACH)
                } else {
                    stopForeground(true)
                }
            }
        }
        val manager = PlayerNotificationManager.Builder(
            this, BuildConfig.NOTIFICATION_ID,
            getString(R.string.app_channelId)
        ).setChannelDescriptionResourceId(R.string.app_channel_description)
            .setChannelImportance(NotificationUtil.IMPORTANCE_HIGH)
            .setSmallIconResourceId(R.drawable.ic_notification)
            .setChannelNameResourceId(R.string.app_name).setMediaDescriptionAdapter(adapter)
            .setNotificationListener(listener).build().apply {
                setUseStopAction(true)
                setUsePreviousAction(true)
                setUseNextAction(true)
                setUseFastForwardAction(false)
                setUseRewindAction(false)
                setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
            }
        player.addListener(object : Player.Listener {
            override fun onPlaybackStateChanged(playbackState: Int) {
                super.onPlaybackStateChanged(playbackState)
                when (playbackState) {
                    Player.STATE_ENDED -> {
                        manager.setPlayer(null)
                    }

                    else -> {
                        manager.setPlayer(player)
                    }
                }
            }
        })
    }
}