package com.mediafileplayer

import androidx.appcompat.app.AppCompatDelegate
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.mediafileplayer.di.component.DaggerAppComponent
import com.mediafileplayer.utils.Logger
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class AppApplication : DaggerApplication() {

    private val appComponent: AndroidInjector<AppApplication> by lazy {
        DaggerAppComponent.factory().create(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return appComponent
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        Logger.LOGGER = BuildConfig.DEBUG
        MobileAds.initialize(this)
        if (BuildConfig.DEBUG)
            MobileAds.setRequestConfiguration(
                RequestConfiguration.Builder().setTestDeviceIds(
                    listOf("89237135C44B05B58B8CAD07F159F6E7")
                ).build()
            )
    }
}