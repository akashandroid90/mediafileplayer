package com.mediafileplayer.di.component

import com.mediafileplayer.AppApplication
import com.mediafileplayer.di.module.ActivityModule
import com.mediafileplayer.di.module.AppModule
import com.mediafileplayer.di.module.FragmentModule
import com.mediafileplayer.di.module.PlayerModule
import com.mediafileplayer.di.module.ServiceModule
import com.mediafileplayer.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 *  initialize modules used for Dagger to provide instance to application
 */
@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ViewModelModule::class, PlayerModule::class, ActivityModule::class, ServiceModule::class, FragmentModule::class])
interface AppComponent : AndroidInjector<AppApplication> {
    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<AppApplication>
}