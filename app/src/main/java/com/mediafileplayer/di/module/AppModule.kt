package com.mediafileplayer.di.module

import android.content.ContentResolver
import android.content.Context
import android.media.MediaMetadataRetriever
import com.mediafileplayer.AppApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * provides application level instances
 */
@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: AppApplication): Context {
        return application.baseContext
    }

    @Provides
    @Singleton
    fun provideContentResolver(context: Context): ContentResolver {
        return context.contentResolver
    }

    @Provides
    @Singleton
    fun provideMediaMetadataRetriever(): MediaMetadataRetriever {
        return MediaMetadataRetriever()
    }
}