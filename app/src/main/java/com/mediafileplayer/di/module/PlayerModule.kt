package com.mediafileplayer.di.module

import android.content.Context
import androidx.annotation.OptIn
import androidx.media3.common.AudioAttributes
import androidx.media3.common.C
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.MediaSource
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import com.mediafileplayer.bean.PlayerBean
import com.mediafileplayer.notification.DescriptionAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * provides application level instances
 */
@Module
class PlayerModule {

    @Provides
    @Singleton
    fun providePlayerTrackList(): MutableList<PlayerBean> {
        return mutableListOf()
    }

    @OptIn(UnstableApi::class)
    @Provides
    @Singleton
    fun provideDescriptionAdapter(context: Context, list: MutableList<PlayerBean>) =
        DescriptionAdapter(context, list)

    @Provides
    @Singleton
    fun provideSimpleExoPlayer(
        context: Context
    ): ExoPlayer {
        return ExoPlayer.Builder(context).build()
            .also {
                it.setAudioAttributes(
                    AudioAttributes.Builder().setUsage(C.USAGE_MEDIA).build(),
                    true
                )
            }
    }

    @OptIn(UnstableApi::class)
    @Provides
    @Singleton
    fun provideContentDataSource(
        context: Context
    ): MediaSource.Factory {
        return ProgressiveMediaSource.Factory(DefaultDataSource.Factory(context))
    }
}