package com.mediafileplayer.di.module

import com.mediafileplayer.notification.PlayerService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    abstract fun contributePlayerService(): PlayerService
}