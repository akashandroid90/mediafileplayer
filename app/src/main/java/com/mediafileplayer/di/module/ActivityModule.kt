package com.mediafileplayer.di.module

import com.mediafileplayer.ui.home.HomeActivity
import com.mediafileplayer.ui.player.musicPlayer.MusicPlayerActivity
import com.mediafileplayer.ui.player.videoPlayer.VideoPlayerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * provides builder methods for Dagger
 */
@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeMusicPlayerActivity(): MusicPlayerActivity

    @ContributesAndroidInjector
    abstract fun contributeVideoPlayerActivity(): VideoPlayerActivity

    @ContributesAndroidInjector
    abstract fun contributeHomeActivity(): HomeActivity
}