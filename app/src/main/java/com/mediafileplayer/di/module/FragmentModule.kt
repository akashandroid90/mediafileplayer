package com.mediafileplayer.di.module

import com.mediafileplayer.ui.list.musicList.TracksFragment
import com.mediafileplayer.ui.list.playList.PlayListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * provides builder methods for Dagger
 */
@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeMusicFragment(): TracksFragment

    @ContributesAndroidInjector
    abstract fun contributePlayListFragment(): PlayListActivity
}