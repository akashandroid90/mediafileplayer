package com.mediafileplayer.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mediafileplayer.di.module.viewmodel.ViewModelFactory
import com.mediafileplayer.di.module.viewmodel.ViewModelKey
import com.mediafileplayer.ui.home.HomeViewModel
import com.mediafileplayer.ui.list.musicList.TrackViewModel
import com.mediafileplayer.ui.list.playList.PlayListViewModel
import com.mediafileplayer.ui.player.musicPlayer.MediaPlayerViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrackViewModel::class)
    internal abstract fun bindMusicViewModel(viewModel: TrackViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MediaPlayerViewModel::class)
    internal abstract fun bindMediaPlayerViewModel(viewModel: MediaPlayerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PlayListViewModel::class)
    internal abstract fun bindMediaPlayListViewModel(viewModel: PlayListViewModel): ViewModel
}