package com.mediafileplayer.bean

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class VideoBean(
    override val rowId: Long,
    override val name: String,
    override val displayName: String,
    override val id: Long = rowId,
    override val count: Int = 0,
    override val mediaPath: String?,
    override val mimeType: String? = null,
    override var duration: Long = 0
) : PlayerBean, Parcelable