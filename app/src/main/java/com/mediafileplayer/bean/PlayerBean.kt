package com.mediafileplayer.bean

interface PlayerBean : MediaBean {
    val displayName: String?
    var duration: Long
}