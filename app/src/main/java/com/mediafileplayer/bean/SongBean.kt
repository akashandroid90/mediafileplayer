package com.mediafileplayer.bean

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SongBean(
    override val rowId: Long,
    override val name: String?,
    override val displayName: String?,
    override val id: Long = rowId,
    override val count: Int,
    override var mediaPath: String? = null,
    override val mimeType: String? = null,
    var artist: String? = null,
    override var duration: Long = 0
) : PlayerBean, Parcelable