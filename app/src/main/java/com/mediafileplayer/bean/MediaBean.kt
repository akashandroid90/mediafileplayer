package com.mediafileplayer.bean

interface MediaBean {
    val rowId: Long
    val name: String?
    val id: Long
    val count: Int
    val mediaPath: String?
    val mimeType: String?
}