package com.mediafileplayer.bean


data class AlbumBean(
    override val rowId: Long,
    override val name: String,
    override val id: Long = rowId,
    override val count: Int = 0,
    override var mediaPath: String? = null,
    override val mimeType: String? = null
) : MediaBean