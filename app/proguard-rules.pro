# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\ide\android\AndroidSDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#-------common rules----

-keep public class * extends androidx.appcompat.app.AppCompatActivity
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends androidx.fragment.app.Fragment

#-------App rules----

-keep public class * extends com.mediafileplayer.ui.base.AppBaseActivity
-keep public class * extends com.mediafileplayer.AppApplication

# -- databinding modules---
-keep public class * extends androidx.databinding.ViewDataBinding


# -- lifecycle modules---
-keep public class * extends androidx.lifecycle.ViewModel
-keep public class * extends androidx.lifecycle.AndroidViewModel

-keepclassmembers class * extends androidx.lifecycle.ViewModel {
    <init>(...);
}
-keepclassmembers class * extends androidx.lifecycle.AndroidViewModel {
    <init>(...);
}
-keepclassmembers class * implements androidx.lifecycle.LifecycleObserver {
    <init>(...);
}
# keep Lifecycle State and Event enums values
#-keepclassmembers class androidx.lifecycle.Lifecycle.State { *; }
#-keepclassmembers class androidx.lifecycle.Lifecycle.Event { *; }
# keep methods annotated with @OnLifecycleEvent even if they seem to be unused
# (Mostly for LiveData.LifecycleBoundObserver.onStateChange(), but who knows)
-keepclassmembers class * {
    @androidx.lifecycle.OnLifecycleEvent *;
}

-keepclassmembers class * implements androidx.lifecycle.LifecycleObserver {
    <init>(...);
}

-keep class * implements androidx.lifecycle.LifecycleObserver {
    <init>(...);
}
-keep class * implements androidx.lifecycle.GeneratedAdapter {<init>(...);}
#-keepclassmembers class androidx.arch.** { *; }
#-keep class androidx.arch.** { *; }
-dontwarn android.arch.**


-dontwarn java.nio.file.*
-dontwarn okio.**




-ignorewarnings
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable
-keepattributes *Annotation*,EnclosingMethod

-optimizations !code/simplification/advanced,code/simplification/*

-keepclassmembers enum * { public static **[] values(); public static ** valueOf(java.lang.String); }

-keep public class * extends android.view.View {
void set*(***);
   *** get*();
   }

-keep class com.bumptech.glide.* { *; }
-keep class androidx.databinding.* { *; }
-keep class androidx.lifecycle.* { *; }

-keep class com.mediafileplayer.* { *; }

-keepattributes RuntimeVisibleAnnotations,AnnotationDefault,Deprecated
-optimizationpasses 5
-verbose

-keepclassmembers class com.google.android.exoplayer2.* {
  <init>(android.content.Context, android.net.Uri);
}

#COID

# ServiceLoader support
-keepnames class kotlinx.coroutines.internal.MainDispatcherFactory {}
-keepnames class kotlinx.coroutines.CoroutineExceptionHandler {}
-keepnames class kotlinx.coroutines.android.AndroidExceptionPreHandler {}
-keepnames class kotlinx.coroutines.android.AndroidDispatcherFactory {}

# Most of volatile fields are updated with AFU and should not be mangled
-keepclassmembernames class kotlinx.* {
    volatile <fields>;
}

# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
#-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

-keepclassmembers class * implements android.os.Parcelable {

    static *** CREATOR;
}
